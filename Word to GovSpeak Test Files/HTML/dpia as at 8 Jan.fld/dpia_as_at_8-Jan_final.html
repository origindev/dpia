
#NHS COVID-19 app: data protection impact assessment

##The NHS COVID-19 app (updated December 2020): data protection impact assessment

##The app and app user in context

The Department of Health and Social Care (DHSC) is the data controller for several services provided to members of the public (data subjects). DHSC is responsible for taking steps to protect the privacy of individuals, to reduce their identifiability and to ensure that processing is proportionate, and that safeguards are in place. For example, as service users pass through systems, this means ensuring that only the minimum amount of data necessary is shared between services and data sets are held separately.

In the context of a pandemic (a public health emergency), data sharing between services is necessary to support the management of communicable diseases and for the protection of the public. This includes monitoring the effectiveness of services and their impacts on different communities.

This DPIA is updated to reflect new functionality introduced to the NHS COVID-19 App (the “app”) which supports the NHS Test, Trace and Protection service in Wales and the NHS Test and Trace service in England. You can find a summary of these changes in the section [Update to the Data Protection Impact Assessment (DPIA) for the NHS COVID-19 App](https://www.gov.uk/government/publications/nhs-covid-19-app-privacy-information/nhs-covid-19-app-data-protection-impact-assessment#updates).

This DPIA is updated to reflect new functionality introduced to the NHS COVID-19 App (the “app”) which supports the NHS Test, Trace and Protection service in Wales and the NHS Test and Trace service in England.

More information our work with health services in Gibraltar, Jersey, Northern Ireland and Scotland can be found in the section [working together](https://www.gov.uk/government/publications/nhs-covid-19-app-privacy-information/nhs-covid-19-app-data-protection-impact-assessment#interoperability).

###The app user (the app data ecosystem)

The use of data collected from app users is subject to the controls and oversight detailed in this DPIA and associated privacy notice.

The law requires a clear commitment from the Department of Health and Social Care to maintain the user’s privacy and protect their identity from other app users and the government. The user journey and data flows are shown in diagrams throughout this document. More detail is provided within the [appendices](https://www.gov.uk/government/publications/nhs-covid-19-app-privacy-information/nhs-covid-19-app-data-protection-impact-assessment#appendices) and annexes of this document.

Any change to the data requested will be reviewed, will be lawful, and will be available as an additional choice by the user. Please note that any future changes beyond the scope of the current version of the app will be reflected and updated in this data protection impact assessment (DPIA).

Data generated and collected by the app is held in 3 environments. Systems are in place which support the secure and appropriate flow of data between these environments:


- app users’ phone – the NHS COVID-19 App and the majority of data collected by the Apple/Google API will be always (and only) held on the app user’s phone. This is considered a user-held record. For most functionality, data is presented to the user’s phone and is checked against the data held on the phone (for example, visited venue QR codes that could be considered at risk or other users that should be considered at risk)


- product environment – certain data items are collected from user devices (via an API) to allow core features of the app to work and be managed effectively. This data collection includes details of the phone type and operating system and the user provided area information (postcode district and local authority). These items are described in the data dictionary set out in [appendix 1 of this document](https://www.gov.uk/government/publications/nhs-covid-19-app-privacy-information/nhs-covid-19-app-data-protection-impact-assessment#appendix1). Within the product environment service performance dashboards are provided to support the oversight and management of the app and associated services. Data and access is kept within the control of the DHSC.


- analytical environment – derived data from the app will flow to the analytical environment to support learning about the app and COVID-19. All data held in the analytical environment is subject to strict de-identification controls to ensure datasets are de-identified and aggregated.

###Virology testing

App users who enter symptoms and are recommended to seek a COVID-19 virology (antigen or swab test can generate a reference code (“test code”) unique to themselves and to the particular occasion on which they seek a test. The test code gives the user access to the virology testing website where they can book a test. The functionality of the app will ensure that the test code is transferred to the website. In order for a test to be booked and the results sent to the correct individual, the website requests additional data from the user.

The test result and code are used to feed results back to the app. This will:


- update the user’s COVID-19 status


- add the relevant code to the list of “at risk” individuals presented to app users’ phones (triggering an update in the status of app users if their “Exposure Log” includes the protected identification code of the individual)

Further use of the data collected as part of the testing process is governed by the terms of use, privacy notice and DPIA of the website the user will be transferred to.

As noted in the update to this DPIA, new functionality allows the user to add a test result into the app without a test code generated from within the app.

###Venue (QR) alerting system

Venue alert data is presented as a list of “at risk” QR codes (protected to obscure the location) sent out by the system to the app. This data triggers the alert functionality within the app.

Using QR codes to check in at venues is a more robust privacy preserving mechanism for the user than manually signing-in with a pen and paper.

This function does not relate directly to the data processed within the app.

To enable users to put this function in context, we set out below how the QR check in function works for venues, and how the process functions end to end below.

Creating a QR poster

Venues are able to [create a NHS Test and Trace QR code by using the GOV service available](https://www.gov.uk/create-coronavirus-qr-poster). The QR generator resides in a sub AWS account of the NHSX domain, distinct from the app.

They will need to provide the following details:


- an email address – used to send a 6-digit verification code, required to continue the QR generation process.


- name – used to personalise the email sent with the link to the poster.


- type of venue – denotes the type of venue which his included as QR codes are not mandatory in all venues.


- venue name – this is displayed on the poster.


- address – displayed on the poster.


- contact email address and phone number for responsible person – used in case of need to contact venue.

An official T&amp;T QR poster will be generated and emailed to the recipient, to be displayed at their venue.

Checking in

An app user is able to check in to a venue by scanning the QR code using the NHS COVID-19 App. When they check in, the venue name, date and time is shown to the user so they can verify the details (and cancel if necessary). The venue name, unique poster ID, date and time are stored on their device for up to 21 days, and this list is visible to the user. The user is able to delete any entries from this list.

If a user scans a third-party QR instead of the official COVID-19 the app will immediately alert the user, advising them the QR code is not recognised and it could be that they didn’t scan an official NHS QR code (or the code is damaged).

Identification of outbreak at a venue

Local Health Protection Teams (HPT) are responsible for local outbreak management. PHE consultants within the HPTs will investigate clusters to determine if there is an outbreak. They may receive information from a venue itself (reporting that staff/customers have tested positive), or through the Contact Tracing and Advice Service (CTAS) information which captures details provided voluntarily by persons who have tested positive, via a contact agent or an online webform. They use existing processes and log details in the HP Zone case management tool. This is outside of the scope of this DPIA.

Triggering the notification in the app

Secure two-factor authentication is used to access the Venue Risk Notification tool. This resides in the sub-AWS account in the NHSX domain, and provides a list of venues which have created a QR poster. In this system it is possible to:


- search by Postcode or Poster ID and select a venue.


- once selected, a date/time from/to will be selected, depending on the assessment they have carried out

When the venue is flagged in the Venue Risk Notification Tool:


- an API call is made into the app backend system.


- the app system sends the list of QR IDs into the app for all app users.


- the app will match against the user’s venue history, and if a match is identified it will trigger a notification to that user.


- the notification does not display the venue name

###Contact tracing (national)

No data from app users is passed to any of the national contract tracing systems in use. App users can choose to use the information held only on their phone to support the contact tracing process when interacting with the service. Data collection and use is governed by the DPIA and privacy notice alongside processes used by that service. No data is shared from the app.

An option being considered is to allow app users the choice to send data held on their phone to contact tracers. This is not currently available as a feature, but if adopted would allow the user to share details of the venues they have visited.

This option is subject to a change control review - including a review of the legality, proportionality and impacts upon privacy and identifiability.

###Contact tracing (local)

The same conditions apply to any local contact tracing by local public health teams. The app user may use the data held only on their phone to assist themselves and the contact tracer, if they choose to.

###Update to the data protection impact assessment (DPIA) for the NHS COVID-19 app (December 2020)

The DPIA is continually updated to reflect changes made to the NHS COVID-19 app and those changes that impact upon the service it provides. In line with GDPR and ICO best practice, the changes are reviewed as they are proposed. This ensures that they remain consistent with the obligation to app users, the law and support the purposes set out for the app.

The publication of this DPIA has occurred at the end of a series of small changes that contribute to the update of the app’s use of data required by:


- changes to the Apple and Google digital contact tracing functionality (the GAEN)


- alignment with wider government policy in providing advice relevant to the Local Authorities. This means app users now select their Local Authority based upon their postcode district


- learning from the app’s initial months of use

Relevant updates for app users to other services

The app now offers support to app users seeking:


- [Test and Trace Support Payments (for app users in England)](https://sip.test-and-trace.nhs.uk/help-support) 


- [Self-isolation support scheme (SISS) (for app users in Wales)](https://gov.wales/self-isolation-support-scheme) 

Outside of the app, the process that assesses whether a venue poses a risk of infection was improved and the administrative burden on Local Health Protection Teams reduced. The updated process does not result in any more data being processed by the app or change any processing.

GAEN Mode 2 (updated digital contact tracing functionality)

The NHS COVID-19 app utilises Apple and Google’s digital contact tracing functionality known as GAEN.

An explanation of the functionality of GAEN can be found in section on the GAEN which includes more detail about these changes, how they affect the app and its use of data.

Apple and Google set conditions for the use of their digital contact tracing technology (the “GAEN”) which ensure that app use is anonymous and the users’ privacy and identity protected. These standards, and the DHSC application of them, are maintained with these changes.

GAEN Mode 2 differs from the previous Mode 1 in the following ways:


- Mode 1 – the existing “bucket” approach for capturing contact events. For example, it would classify distance of contacts into “buckets” defined as near, medium or far


- Mode 2 – uses the measurements captured by the GAEN rather than these approximations


- the update utilises an Exposure Window data set as part of calculating the risk of infection and whether an app user should be alerted as a result of a risky contact (and a high risk of infection)


- this Exposure Window is available for those providing COVID-19 apps, to support their analysis and public health purposes


- Crucial to the risk calculation is the infectiousness of the index case (the app user with COVID-19 who has updated their status and shared their Diagnosis Keys). This is calculated from the onset of symptoms data which accompanies the Diagnosis Keys when they are shared


- Further uses of the Exposure Window data set are expected in due course and further details can be found in this DPIA

This change reflects improvements to Google, Apple and our understanding of the Low Energy Bluetooth technology, COVID-19 and the public health emergency.

Detail on the algorithm and science behind these changes is available in the updated summary of the risk algorithm and within the [Head of Data and Analytics for the app blog here](https://www.turing.ac.uk/blog/updates-algorithm-underlying-nhs-covid-19-app) 

These changes impact upon both the data generated by GAEN and that which is utilised by the app for both Modes 1 and 2:

The changes required a review of the data used by the app, and in the analytical data set.

The data set is used to demonstrate that the app is working and contributes to the purposes set out in this DPIA:


- In particular, the public health purposes of the app – breaking the transmission of the COVID-19 virus

Crucially, this also allowed us to refine the approach based on the learning from app users, ongoing testing and the latest scientific advice.

More detail on these changes can be found in the updated section on Digital Contact Tracing and throughout this DPIA.

Review of the Analytical Data Set

The data items used within the app (as well as the analytical data set that supports it) have required an update due to:


- ongoing learning from the working of the app


- the performance of technology


- updates from Apple and Google

This review is ongoing and will look to conclude early in 2021. Depending on future developments in COVID-19, it may be necessary to review as a result of:


- changes made by Apple and Google to their digital contact tracing technology and phone Operating Systems (iOS and Android)


- our improvements to the app, support for other functions and options for app users


- what we learn about COVID-19 and the public health response

Changes to the analytical data set for this update include:


- updating the Test Result and Reason data set to count which testing journey is being used by app users


- updating the Isolation analytical data set to count the reason for any recommendation for any isolation


- update to provide a clearer service overview by counting the number of exposure notifications sent by app users

All these changes must conform to the existing requirements of the app, the limitations of data and protections of app user’s privacy and identity. The data set includes only a count of most data items and only covers a 24-hour period. For more information, see [Appendix 1 (Data Dictionary)](https://www.gov.uk/government/publications/nhs-covid-19-app-privacy-information/nhs-covid-19-app-data-protection-impact-assessment#appendix1).

Addition of local authorities

Government policy provides advice to the public on a Local Authority level. The NHS COVID-19 app is moving to align with this approach and will utilise both the user’s Postcode District and Local Authority to provide updates to the user:


- when entering their postcode district, app users will also be asked to select their relevant local authority


- the app will use this data to provide the latest information for that area, for example, an alert to a change in risk status

In due course, the local authority data item will be added to the analytical data set, as part of wider alignment and consistency.

Processing both the Local Authority and Postcode District for a user increases the risk of reidentification. The processes used to reduce this risk can be found in the privacy issues identified and risk analysis(#riskregister).

Users seeking Isolation Support Payment

The NHS COVID-19 app will now allow app users to claim for a support payment if they receive advice to isolate based on interaction with an app user who subsequently confirms positive with COVID-19.

To make a claim, the app user will be prompted to leave the NHS COVID-19 app and enter a secure NHS portal where details about their claim will be captured.

Details about the terms and conditions, as well as the use of their data can be found in the [portal and contact tracing privacy notice](https://contact-tracing.phe.gov.uk/help/privacy-notice).

The app will generate a validation token. This demonstrates that they have had a genuine alert from the app and are eligible to request a payment.

More information on how this token is generated and how the privacy and identity of the app user is protected within the app, can be found in the [section ‘Isolation support payment’](https://www.gov.uk/government/publications/nhs-covid-19-app-privacy-information/nhs-covid-19-app-data-protection-impact-assessment#isp).

In addition to the token, the app provides the encounter date and end of isolation period date for the user – to ensure that any self-isolation is aligned with the Contact Tracing system (CTAS).

The app does not receive, utilise or retain any data about an app user’s self-isolation claim other than to validate the token provided.

Points of clarification

The intention to collect the analytical data set every 6-hours is not currently in place, and the data set continues to be collected daily. The DPIA is updated to reflect the daily data collection, the event analytical data set and the relevant risks were reviewed, see [the risk register](https://www.gov.uk/government/publications/nhs-covid-19-app-privacy-information/nhs-covid-19-app-data-protection-impact-assessment#riskregister).

New functionality

See above for the new functions that support the purposes of the app and the app user.

Next steps for the app and the DPIA

The DPIA for the app continues to be subject to routine review, as significant changes are identified the DPIA is updated. A new version of the DPIA will be published when major updates to the app are released.

###Apple and Google

The NHS COVID-19 App is available through the Apple App Store and the Google Play Store. Apple and Google provide the app independently of the NHS and Department of Health and Social Care.

For app user’s using older versions of the app, Apple and Google are responsible for exposure notification messages sent to users which are outside of the remit and control of the NHS and DHSC. In response to this notification, the NHS COVID-19 app provides a clarification message to explain the level of risk and actions recommended as a consequence.

We recommend routinely updating the app to ensure you have the latest functionality and best performance of the NHS COVID-19 app.

For more information about Apple and Google’s digital contact tracing technology [see their websites](https://covid19.apple.com/contacttracing).



##The NHS Test and Trace app: data protection impact assessment

##Introduction

The NHS COVID-19 app (“app”) is a mobile phone application that is part of the NHS Test and Trace Service, which is designed to break the chains of transmission of COVID-19. By using the app, users will help to protect themselves and those around them – their friends, family, colleagues and local communities, and enable society to return to a more normal way of life. The app is a medical device providing maximum freedom at minimum risk.

The objectives of the app are to:


- create an enduring new medical technology to manage public health


- promote behaviour change by helping people manage their risk exposure


- identify and inform people to help communities manage public health emergencies


- reduce disease transmission by giving users easy access to health services


- support and inform users during isolation

The behaviour sought by the app from users is to:


- download the app and use it daily


- keep the app ‘on’ and carry their phone at all times


- follow instructions issued by the app


- ‘pause’ the app when appropriate


- enter symptoms and take a test quickly when told to


- self-isolate (as we expect of everyone) if a user tests positive for COVID-19

Any “pause” of contact tracing needs to be resumed manually by the user. This is not an automated function.

The NHS COVID-19 App has 3 core roles:


- precision: to measure distance and time between app users accurately


- reach: to remember which other app users an app user has been near


- speed: to initiate self-isolation quickly through contact detection of positive cases

(Image number)

Figure 1: The 3 core roles

